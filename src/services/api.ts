import { useEffect, useState } from 'react'
import {
  ApiResponse,
  UserInfosData,
  PerformanceData,
  AverageData,
  ActivityData,
} from '../types'
import {
  formatUserAverage,
  formatUserInfos,
  formatUserPerformance,
} from './utils.ts'
import { useUserContext } from '../context/userContext.tsx'

function useUserInfos() {
  const { data, ...hook } = useFetch<UserInfosData>('')
  return { user: data ? formatUserInfos(data) : null, ...hook }
}

function useUserPerformance() {
  const { data, ...hook } = useFetch<PerformanceData>('performance')
  return { performance: data ? formatUserPerformance(data) : null, ...hook }
}

function useUserAverage() {
  const { data, ...hook } = useFetch<AverageData>('average-sessions')
  return { average: data ? formatUserAverage(data) : null, ...hook }
}

function useUserActivity() {
  const { data, ...hook } = useFetch<ActivityData>('activity')
  return { activity: data, ...hook }
}

function useFetch<T>(path: string): ApiResponse<T> {
  const [data, setData] = useState<T | null>(null)
  const [error, setError] = useState<Error | null>(null)
  const [loading, setLoading] = useState<boolean>(true)

  const { mocked, id } = useUserContext()

  if (path === '' && mocked) {
    path = 'infos'
  }

  const url = mocked
    ? `http://localhost:5173/user_${id}/${path}.json`
    : `http://localhost:3000/user/${id}/${path}`

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => setData(data.data))
      .catch((error) => {
        console.error('Erreur lors de la récupération des données:', error)
        setError(error.message)
      })
      .finally(() => setLoading(false))
  }, [path, url])

  return { data, error, loading }
}

export { useUserInfos, useUserPerformance, useUserAverage, useUserActivity }
