import {
  Average,
  AverageData,
  UserInfos,
  UserInfosData,
  Performance,
  PerformanceData,
  PerformanceType,
} from '../types'

export function formatUserInfos(user: UserInfosData): UserInfos {
  return {
    id: user.id,
    userInfos: {
      firstName: user.userInfos.firstName,
      lastName: user.userInfos.lastName,
      age: user.userInfos.age,
    },
    keyData: {
      calorieCount: user.keyData.calorieCount,
      proteinCount: user.keyData.proteinCount,
      carbohydrateCount: user.keyData.carbohydrateCount,
      lipidCount: user.keyData.lipidCount,
    },
    score: user.score ?? user.todayScore ?? 0,
  }
}

export function formatUserAverage(average: AverageData): Average[] {
  const days = ['L', 'M', 'M', 'J', 'V', 'S', 'D']

  const elements = []
  for (const session of average.sessions) {
    elements.push({
      day: days[session.day - 1],
      sessionLength: session.sessionLength,
    })
  }

  return elements
}

export function formatUserPerformance(
  performance: PerformanceData
): Performance[] {
  const translation: Record<PerformanceType['kind'], string> = {
    1: 'Intensité',
    2: 'Vitesse',
    3: 'Force',
    4: 'Endurance',
    5: 'Énergie',
    6: 'Cardio',
  }

  const elements: Performance[] = []
  for (const data of performance.data) {
    elements.push({
      subject: translation[data.kind],
      value: data.value,
    })
  }

  return elements
}
