import {
  createContext,
  FC,
  PropsWithChildren,
  useContext,
  useState,
} from 'react'

type Context = {
  id: number
  setId: (id: number) => void
  mocked: boolean
  setMocked: (mocked: boolean) => void
}

const DEFAULT_VALUES = {
  id: 12,
  setId: () => {},
  mocked: true,
  setMocked: () => {},
}

const UserContext = createContext<Context>(DEFAULT_VALUES)

export const UserContextProvider: FC<PropsWithChildren> = ({ children }) => {
  const [id, setId] = useState<number>(DEFAULT_VALUES.id)
  const [mocked, setMocked] = useState<boolean>(DEFAULT_VALUES.mocked)

  return (
    <UserContext.Provider value={{ id, setId, mocked, setMocked }}>
      {children}
    </UserContext.Provider>
  )
}

export const useUserContext = () => {
  return useContext(UserContext)
}
