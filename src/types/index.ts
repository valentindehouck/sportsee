export type UserInfosData = {
  id: number
  userInfos: {
    firstName: string
    lastName: string
    age: number
  }
  keyData: {
    calorieCount: number
    proteinCount: number
    carbohydrateCount: number
    lipidCount: number
  }
  score?: number
  todayScore?: number
}

export type UserInfos = {
  id: number
  userInfos: {
    firstName: string
    lastName: string
    age: number
  }
  keyData: {
    calorieCount: number
    proteinCount: number
    carbohydrateCount: number
    lipidCount: number
  }
  score: number
}

export type PerformanceType = {
  kind: number
  value: number
}

export type PerformanceData = {
  data: Array<PerformanceType>
  kind: Array<string>
  userId: number
}

export type Performance = {
  subject: string
  value: number
}

type AverageSessions = {
  day: number
  sessionLength: number
}

export type AverageData = {
  userId: number
  sessions: Array<AverageSessions>
}

export type Average = {
  day: string
  sessionLength: number
}

type Activity = {
  day: string
  kilogram: number
  calories: number
}

export type ActivityData = {
  userId: number
  sessions: Array<Activity>
}

export type ApiResponse<T> = {
  error: Error | null
  data: T | null
  loading: boolean
}
