import { Home } from './views'
import { Header, Sidebar } from './components'
import { UserContextProvider } from './context/userContext.tsx'

function App() {
  return (
    <UserContextProvider>
      <Header />
      <main>
        <Sidebar />
        <Home />
      </main>
    </UserContextProvider>
  )
}

export default App
