import { apple, chicken, energy, cheeseBurger } from '../../assets/icons'
import './style.scss'
import { useUserInfos } from '../../services/api.ts'
import { Health, Score, Performance, Average, Activity } from '../../components'

function Home() {
  const { user, error, loading } = useUserInfos()

  if (user === null) {
    return null
  }

  if (loading) {
    return <h1>Chargement...</h1>
  }

  if (error) {
    return (
      <div className="home">
        <h1>Oops ! Une erreur s'est produite</h1>
        <button onClick={() => window.location.reload()}>
          Recharger la page
        </button>
      </div>
    )
  }

  return (
    <div className="home">
      <div className="home__welcome">
        <h1>
          Bonjour{' '}
          <span className="home__welcome_name">{user.userInfos.firstName}</span>
        </h1>
        <p>Félicitations ! Vous avez explosé vos objectifs hier 👏</p>
      </div>
      <div className="home__content">
        <div className="home__charts">
          <div className="home__activity">
            <Activity />
          </div>
          <div className="home__average">
            <Average />
          </div>
          <div className="home__performance">
            <Performance />
          </div>
          <div className="home__score">
            <Score score={user.score} />
          </div>
        </div>
        <div className="home__health">
          <Health
            icon={energy}
            label="Calories"
            value={user.keyData.calorieCount}
            unit="kCal"
          />
          <Health
            icon={chicken}
            label="Protéines"
            value={user.keyData.proteinCount}
            unit="g"
          />
          <Health
            icon={apple}
            label="Glucides"
            value={user.keyData.carbohydrateCount}
            unit="g"
          />
          <Health
            icon={cheeseBurger}
            label="Lipides"
            value={user.keyData.lipidCount}
            unit="g"
          />
        </div>
      </div>
    </div>
  )
}

export default Home
