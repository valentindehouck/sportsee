import './style.scss'
import {
  Legend,
  RadialBar,
  RadialBarChart,
  ResponsiveContainer,
} from 'recharts'

type ScoreProps = {
  score: number
}

function Score({ score }: ScoreProps) {
  const data = [
    {
      x: score * 100,
      fill: '#FF0101',
    },
    {
      x: 100,
      fill: 'transparent',
    },
  ]

  return (
    <>
      <h2 className="score__title">Score</h2>
      <ResponsiveContainer width="100%" height={165}>
        <RadialBarChart
          innerRadius="82%"
          outerRadius="110%"
          startAngle={90}
          endAngle={450}
          data={data}
        >
          <RadialBar dataKey="x" cornerRadius={10} />
          <Legend
            layout="vertical"
            verticalAlign="middle"
            content={() => (
              <div className="score__legend">
                <span className="score__legend_pourcentage">
                  {score * 100}%
                </span>
                <p className="score__legend_text">de votre objectif</p>
              </div>
            )}
          />
        </RadialBarChart>
      </ResponsiveContainer>
    </>
  )
}

export default Score
