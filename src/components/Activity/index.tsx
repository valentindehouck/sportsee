import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import './style.scss'
import { useUserActivity } from '../../services/api.ts'

const tooltipContent = (name: string, value: number) => {
  switch (name) {
    case 'Poids (kg)':
      return `${value} kg`
    case 'Calories brûlées (kCal)':
      return `${value} kCal`
  }
}

function Activity() {
  const { activity, error, loading } = useUserActivity()

  if (activity === null) {
    return
  }

  if (loading) {
    return <p>Chargement...</p>
  }

  if (error) {
    return <p>Oops ! Une erreur s'est produite</p>
  }

  return (
    <>
      <h2 className="activity__title">Activité quotidienne</h2>
      <ResponsiveContainer width="100%" height="90%">
        <BarChart data={activity.sessions} barCategoryGap="30%">
          <Tooltip
            contentStyle={{
              backgroundColor: '#FF0101',
              border: 'none',
            }}
            cursor={{ fill: '#C4C4C4', opacity: 0.5 }}
            itemStyle={{ color: 'white' }}
            labelFormatter={() => ''}
            separator={''}
            formatter={(value: number, name: string) => [
              tooltipContent(name, value),
              '',
            ]}
          />
          <Legend
            verticalAlign="top"
            align="right"
            wrapperStyle={{ marginTop: '-40px' }}
            iconType="circle"
            iconSize={8}
          />
          <CartesianGrid strokeDasharray="2" vertical={false} />
          <XAxis
            dataKey="day"
            tickFormatter={(tick) => new Date(tick).getDate().toString()}
            tickLine={false}
            tick={{ fill: '#9B9EAC' }}
            stroke={'#DEDEDE'}
          />
          <YAxis
            domain={[0, 'dataMax + 50']}
            axisLine={false}
            tickLine={false}
            orientation={'right'}
            tick={{ fill: '#9B9EAC' }}
          />
          <Bar
            dataKey="kilogram"
            name="Poids (kg)"
            fill="#282D30"
            radius={[10, 10, 0, 0]}
            maxBarSize={10}
          />
          <Bar
            dataKey="calories"
            name="Calories brûlées (kCal)"
            fill="#E60000"
            radius={[10, 10, 0, 0]}
            maxBarSize={10}
          />
        </BarChart>
      </ResponsiveContainer>
    </>
  )
}

export default Activity
