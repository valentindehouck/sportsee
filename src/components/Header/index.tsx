import logo from '../../assets/logo.png'
import './style.scss'
import { useUserContext } from '../../context/userContext.tsx'

function Header() {
  const { setId, setMocked, mocked } = useUserContext()

  return (
    <header className="header">
      <a className="header__logo" href="#">
        <img src={logo} alt="logo" />
      </a>
      <select onChange={(e) => setId(parseInt(e.target.value))}>
        <option value="12">Karl (id 12)</option>
        <option value="18">Cecilia (id 18)</option>
      </select>
      <input
        type="checkbox"
        id="mock"
        checked={mocked}
        onChange={() => setMocked(!mocked)}
      />
      <label htmlFor="mock" className="header__mocked">
        Mock
      </label>
      <nav className="header__navigation">
        <a href="#" className="header__navigation_link">
          Accueil
        </a>
        <a href="#" className="header__navigation_link">
          Profil
        </a>
        <a href="#" className="header__navigation_link">
          Réglages
        </a>
        <a href="#" className="header__navigation_link">
          Communauté
        </a>
      </nav>
    </header>
  )
}

export default Header
