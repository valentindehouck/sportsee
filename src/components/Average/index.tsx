import { useUserAverage } from '../../services/api.ts'
import {
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import './style.scss'

type Points = {
  x: number
  y: number
}

type CustomCursorProps = {
  points?: Array<Points>
}

const CustomCursor = ({ points }: CustomCursorProps) => {
  if (points === undefined) {
    return null
  }
  return (
    <rect
      fill="black"
      opacity={0.1}
      x={points[0].x}
      height="100%"
      width="100%"
    />
  )
}

function Average() {
  const { average, error, loading } = useUserAverage()

  if (average === null) {
    return
  }

  if (loading) {
    return <p>Chargement...</p>
  }

  if (error) {
    return <p>Oops ! Une erreur s'est produite</p>
  }

  return (
    <>
      <h2 className="average__title">Durée moyenne des sessions</h2>
      <ResponsiveContainer width="100%" height="100%">
        <LineChart data={average}>
          <XAxis
            dataKey="day"
            stroke="#FFFFFF"
            axisLine={false}
            tickLine={false}
            padding={{ left: 10, right: 10 }}
            opacity={0.5}
            tick={{ fontSize: 12 }}
          />
          <YAxis hide domain={[0, 100]} />
          <Tooltip
            labelFormatter={() => ''}
            separator={''}
            formatter={(value) => [`${value} min`, '']}
            itemStyle={{ color: 'black' }}
            cursor={<CustomCursor />}
          />
          <Line
            type="monotone"
            dataKey="sessionLength"
            stroke="#FFFFFF"
            strokeWidth={3}
            dot={false}
            activeDot={{
              r: 5,
              strokeWidth: 12,
              fill: 'white',
              stroke: '#FFFFFF33',
            }}
          />
        </LineChart>
      </ResponsiveContainer>
    </>
  )
}

export default Average
