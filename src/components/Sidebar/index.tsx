import { alters, bike, swimming, yoga } from '../../assets/icons.tsx'
import './style.scss'

function Sidebar() {
  return (
    <div className="banner">
      <div className="banner__container">
        <div className="banner__container_icon">
          <img src={yoga} alt="icon1" />
        </div>
        <div className="banner__container_icon">
          <img src={swimming} alt="icon2" />
        </div>
        <div className="banner__container_icon">
          <img src={bike} alt="icon3" />
        </div>
        <div className="banner__container_icon">
          <img src={alters} alt="icon4" />
        </div>
      </div>
      <p className="banner__copyright">Copyright, SportSee 2020</p>
    </div>
  )
}

export default Sidebar
