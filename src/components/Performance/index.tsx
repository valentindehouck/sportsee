import { useUserPerformance } from '../../services/api.ts'
import {
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart,
  ResponsiveContainer,
} from 'recharts'

function Performance() {
  const { performance, error, loading } = useUserPerformance()

  if (performance === null) {
    return
  }

  if (loading) {
    return <p>Chargement...</p>
  }

  if (error) {
    return <p>Oops ! Une erreur s'est produite</p>
  }

  return (
    <ResponsiveContainer width="100%" height="100%">
      <RadarChart data={performance} cx="50%" cy="50%">
        <PolarGrid stroke="#fff" radialLines={false} />
        <PolarRadiusAxis
          domain={[0, 300]}
          axisLine={false}
          tick={false}
          tickCount={5}
        />
        <PolarAngleAxis
          dataKey="subject"
          tick={{ fill: '#FFF', fontSize: '12px' }}
        />
        <Radar dataKey="value" fill="#FF0101" fillOpacity={0.7} />
      </RadarChart>
    </ResponsiveContainer>
  )
}

export default Performance
