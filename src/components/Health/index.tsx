import './style.scss'

type HealthProps = {
  icon: string
  label: string
  value: number
  unit: string
}

function Health({ icon, label, value, unit }: HealthProps) {
  return (
    <div className="health">
      <div className="health__illustration">
        <img src={icon} alt="" />
      </div>
      <div className="health__content">
        <span className="health__content_value">
          {value}
          {unit}
        </span>
        <p className="health__content_label">{label}</p>
      </div>
    </div>
  )
}

export default Health
