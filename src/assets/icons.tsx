import alters from './alters.svg'
import apple from './apple.svg'
import bike from './bike.svg'
import cheeseBurger from './cheeseBurger.svg'
import chicken from './chicken.svg'
import energy from './energy.svg'
import swimming from './swimming.svg'
import yoga from './yoga.svg'

export { alters, apple, bike, cheeseBurger, chicken, energy, swimming, yoga }
